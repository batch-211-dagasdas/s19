console.log('hello world')

// what are conditional statements?
	// conditional statement allow us to control the flow of our program
	// it allow us to run a statement/instruction if a condition is met or run anothe separate statement if otherwise

// if,else if and else statement

	let numA = -1;

	// if statement execute a statement if a specified condition is true

		if(numA<0){
			console.log("yes its less than zero")
		};
	/*
	Syntax:
		if(condition){
			statement
		}
	*/
	// the result of the expression added in the if's confition must result to true, else, the statement inside if() will not run

	console.log(numA<0);//results to true -- the if statment will rin

	numA = 0

	if(numA<0){
		console.log('helo')
	};
	console.log(numA<0);//false

	let city = "new York";

	if(city === "new York"){
		console.log('welcome to NY')
	}

	// else if clause

	/*
		this execute a statement if previous condition are false and if the specified condition is true
		-the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
	*/
		let numH = 1;
		if (numA<0){
			console.log("hellow")
		} else if (numH>0){
			console.log('world')
		}

	// we were able to run the else if() statement after we evaluated that the if condition was failed
	// if the if() condition was passed and run, we will no longer evaluate the else if() and the process there

	 numA = 1;
		if (numA<0){
			console.log("hellow")
		} else if (numH>0){
			console.log('world')
		}


		city = 'tokyu';
		if (city === "New york"){
			console.log("welcom to New york city")
		} else if (city==="tokyu"){
			console.log("welcom to tokyu japan")
		}
		// since we failed the condition for the first if(), we went to the else if() and checked and instead passed the condition

		// esle statement

		/*
			-executed a statement if all the other conditions are false
			-the 'else' statement is optional and can be added to capture any other result to change the flow of the program
		*/

		if (numA<0){
			console.log("hello if")
		} else if(numH===0) {
			console.log("hello else if")
		}else{
			console.log("else")
		}

		/*
			since the both the preceeding if and else if condition are not met/failed, the else statement was run instead

			else statement should only be added if there is a preceeding if condition
			else statements by itself will not work, howeve, if statements will work eveb if there is no else statement
		*/

		// else if (numH === 0){
		// 	console.log('world')
		// }else{
		// 	console.log('again')
		// }

		//  same goes for an else if , there should be a preceding if() 1st

		//  if, else if and else statements w/ functions
		/*
			-MOST  of the time, we woulf like to use if , else if, and else
			statement w/ function to control the flow of the application
		*/

		let message ="No message."
		console.log(message);

		function determineTyphoonInternsity(windSpeed){
			if(windSpeed<30){
				return "not a Typhoon yet. "
			} else if (windSpeed<=61){
				return "tropical depression detected"
			}else if(windSpeed>=62 && windSpeed<=88){
				return 'tropical storm detected.'
			}else if(windSpeed >=89 || windSpeed <= 177){
				return "Severe tropical storm detected"
			} else{
				return "Typhoon detected"
			}


		}
		// this returns the strings to the variable "message" that invoked it
		message = determineTyphoonInternsity(110);
		console.log(message)


		if (message=="Severe tropical storm detected"){
			console.warn(message);
		}


		// function oddOrEvenChecker(num){
		// 	if(num % 2 === 0){
		// 		alert(num+" is even")
		// 	}else{
		// 		alert(num + " is odd")
		// 	}
		// }

		// oddOrEvenChecker(1);


		// function ageChecker(age){
		// 	 if(age < 17){
		// 	 alert( age + 'not allowed to dring')
		// 	 return false;
		// 	} else {
		// 	alert( age + 'you can Drink')
		// 	return true;
		// 	}
		
		// } 
		 
		// let isAllowedToDrink = ageChecker(19);
		// 	console.log(isAllowedToDrink)

	// truthy and falsy
	/*
		-JS a "truthy" value is a value that is considered true when encountered is boolean context
		-values in are considered true unless defined ptherwise
		-
		falsy values /exceptoons for trusth:
		1.false
		2.0
		3.-0
		4.""
		5.null
		6. undefined
		7.NaN

	*/

	// truthy example
	/*
		-if the result of an expression is a condition resutls to a tr

	*/
	if(" "){
		console.log('truthy')
	}
	if(1){
		console.log('truthy')
	}
		
	if([]){
		console.log('truthy')
	}

	//falsy

	if(false){
		console.log('falsy')
	}
	if(0){
		console.log('falsy')
	}
	
	if(undefined){
		console.log('falsy')
	}

	// conditional (ternary) Operator
		/*
			-the conditoinal or ternary operator in 3 operands
				1.condition
				2.expression to execute if the conditionit truthy
				3.expression to execute if the conditionit falsy
			-this can be used as an alternative of an "if else" statement
			- ternary operator have an implicit return statement mean that w/out the return keyword the resulting expression can be stored in a variable

			syntax: (expression) ? ifTrue : ifFalse;
		*/

	// single statement execution
		// let ternaryResult =(1<18)?true:false;
		// console.log("the result of ternary operator " +ternaryResult)
		// console(ternaryResult);

	// multi statement execution
	/*
		a functoin may b defined then used in a ternaru operator
	*/

	let name;

	function isOfLegalAge(){
		name = "john"
		return ' you are of the legal age'
	}

	function isUnderAge(){
		name = "jane"
		return ' you are under age limit'
	}


	let age = parseInt(prompt("what is your age"));
		console.log(age)
	let isLegalAge = (age>18) ? isOfLegalAge() : isUnderAge();
	console.log("result of ternary Operator in the function: " + isLegalAge + ',' + name)

		
	// switch statement

	/*
	the switch statement evaluates an expression and matches the expression value to a case clause
	*/

	let day = prompt("what day of the week is it today?").toLowerCase();
		console.log(day)

	switch(day) {
		case 'monday':
			console.log('the color of the day is blue');
			break;
		case 'TUEDAY':
			console.log('the color of the day is yellow');
			break;
		case 'wednesday':
			console.log('the color of the day is green');
			break;
		case 'thursday':
			console.log('the color of the day is pink');
			break;
		case 'friday':
			console.log('the color of the day is black');
			break;

		case 'saturday':
			console.log('the color of the day is dark');
			break;

		case 'sunday':
			console.log('the color of the day is asdas');
			break;

			default:
				console.log("please input valid day");
			break
	}	

	function determineBear(bearNumber){
		let bear;

		switch(bearNumber){
			case 1:
			alert('Hi, Im Amy!');
			break;
		case 2:
			alert('Hey, Im lulu!');
			break;
		case 3:
			alert('Hi, Im morgan!');
			break;
		default:
				console.log("bear number " + bear + 'is out of bounds!');
			break
		}
		 return bear;
		}

		determineBear(1);

//  try-catch-finally Statement

	/*
		-"try catch " statement are commonly used for error handling
		-they are used to specify a response whenever an error is reveived
	
	*/

	function showIntensityAlert(windSpeed){
		try{
			aleart(determineTyphoonInternsity(windSpeed))
			// error/err are commonly used variable names used by dev for storing errors
		} catch (error){
			console.log(typeof error);
			// the error.message is used to access the information relating to an error object
			console.warn(error.message)
		}finally{
			// continue execution of code regardless of success and failure of code execution in th 'try' block to handle/resolve errors
			alert('intensity updates will show new alerts')
		}
	}

	showIntensityAlert(56);
		
